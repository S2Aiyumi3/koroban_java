import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    WebDriver driver;
    @BeforeSuite
    public void openChrome() {
        System.setProperty("webdriver.chrome.driver", "G:\\IT\\IV\\Автотестирование ПО\\testWeb\\src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        System.out.println("запуск драйвера хрома завершен");

        driver.manage().window().maximize();
        System.out.println("развертывание окна браузера на максимум завершено");
    }
    @BeforeClass
    public void openUrlTapCookies() throws IOException {
        String url = "https://wearekpop.com/";
        driver.get(url);
        System.out.println("открытие сайта завершено");

        TakesScreenshot scr = ((TakesScreenshot) driver);
        File file = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("G:\\IT\\IV\\Автотестирование ПО\\testWeb\\src\\main\\cookieshow.PNG"));
        System.out.println("скрин cookie m сделан");

        WebElement el = driver.findElement(By.xpath("//a[@class='btn-accept']"));
        el.click();
        scr = ((TakesScreenshot) driver);
        file = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("G:\\IT\\IV\\Автотестирование ПО\\testWeb\\src\\main\\nocookies.PNG"));
        System.out.println("скрин no cookie m сделан");

        System.out.println("приняты куки");
    }
    @AfterClass
    public void closeUp() {
        System.out.println("test of adding 2 items into cart & deletion 1 item from cart is finished");
        driver.quit();
        System.out.println("завершение работы драйвера");
    }
    @AfterSuite
    public void finish() {
        System.out.println("все закрылось");
    }

}

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Main extends BaseTest{
    @BeforeMethod
    public void helloTest()  {
        System.out.println("~✨🌸hello from Test🌸✨~");
    }
    @AfterMethod
    public void screenShotResult() throws IOException {
        TakesScreenshot scr = ((TakesScreenshot) driver);
        File file1 = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file1, new File("G:\\IT\\IV\\Автотестирование ПО\\testWeb\\src\\main\\test.PNG"));
        System.out.println("скрин результата сделан");
    }
    @Test
    public void add2Delete1itemCart() throws InterruptedException, IOException {
        WebElement el = driver.findElement(By.xpath("//button[@data-form-id='#-6883043541188']"));
        Actions onmouseHover = new Actions(driver);
        onmouseHover.moveToElement(el).click(el).release(el).perform();
        TakesScreenshot scr = ((TakesScreenshot) driver);
        File file = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("G:\\IT\\IV\\Автотестирование ПО\\testWeb\\src\\main\\aespaselected.PNG"));
        System.out.println("скрин aespa item is selected сделан");
        Thread.sleep(2000);
        Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'add-ajax-success-modal')]")).getAttribute("style").contains("block"), "steps: #1");

        el = driver.findElement(By.xpath("//a[@href='/collections/goods']"));
        el.click();
        scr = ((TakesScreenshot) driver);
        file = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("G:\\IT\\IV\\Автотестирование ПО\\testWeb\\src\\main\\goodsselected.PNG"));
        System.out.println("скрин goods is selected сделан");
        String currentUrl = driver.getCurrentUrl();
        boolean actual = false;
        for(int i = 0; i<3; i++){
            if(driver.getCurrentUrl().equals("https://wearekpop.com/collections/goods")){
                actual = true;
                break;
            }
            Thread.sleep(400);
        }
        Assert.assertEquals(currentUrl, "https://wearekpop.com/collections/goods", "steps: #2");

        el = driver.findElement(By.xpath("//div[@class='widget-content']//a[@href='/collections/txt']/span"));
        el.click();
        scr = ((TakesScreenshot) driver);
        file = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("G:\\IT\\IV\\Автотестирование ПО\\testWeb\\src\\main\\txtselected.PNG"));
        System.out.println("скрин txt is selected сделан");
        currentUrl = driver.getCurrentUrl();
        for(int i = 0; i<3; i++){
            if(driver.getCurrentUrl().equals("https://wearekpop.com/collections/txt")){
                actual = true;
                break;
            }
            Thread.sleep(400);
        }
        Assert.assertEquals(currentUrl, "https://wearekpop.com/collections/txt", "steps: #3");

        el = driver.findElement(By.xpath("//*[@id=\"shopify-section-collection-template-default\"]/div/div[2]/div[6]/div/div/div[1]/div[1]/a/img"));
        Thread.sleep(400);
        onmouseHover = new Actions(driver);
        onmouseHover.moveToElement(el).click().release().perform();
        scr = ((TakesScreenshot) driver);
        file = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("G:\\IT\\IV\\Автотестирование ПО\\testWeb\\src\\main\\txtalbumselected.PNG"));
        System.out.println("скрин txt album is selected сделан");
        Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class,'ajax-quickview')]")).getAttribute("style").contains("block"), "steps: #4");

        el = driver.findElement(By.xpath("//div[@class='halo-modal-body']//select[@id='product-select-qv-option-0']/option[3]"));
        el.click();
        scr = ((TakesScreenshot) driver);
        file = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("G:\\IT\\IV\\Автотестирование ПО\\testWeb\\src\\main\\txtalbumquickview.PNG"));
        System.out.println("скрин txt album quick view сделан");
        Assert.assertTrue(el.isSelected(), "steps: #5");

        el = driver.findElement(By.xpath("//div[@class='halo-modal-body']//div[@class='qty-group']/a[contains(@class, 'plus')]"));
        el.click();
        el.click();
        Assert.assertTrue(driver.findElement(By.xpath("//*[@id=\"add-to-cart-quickview-form\"]/div[2]/div/input")).getAttribute("value").contains("3"), "steps: #6");

        el = driver.findElement(By.xpath("//div[@class='halo-modal-body']//input[@data-form-id='#add-to-cart-quickview-form']"));
        el.click();
        scr = ((TakesScreenshot) driver);
        file = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("G:\\IT\\IV\\Автотестирование ПО\\testWeb\\src\\main\\txtaddtocart.PNG"));
        System.out.println("скрин txt add to cart is selected сделан");
        Thread.sleep(2000);
        Assert.assertTrue(driver.findElement(By.xpath("//div[contains(@class, 'add-ajax-success-modal')]")).getAttribute("style").contains("block"), "steps: #7");

        el = driver.findElement(By.xpath("//button[contains(@class, 'btn-go-to-cart')]/span"));
        el.click();
        scr = ((TakesScreenshot) driver);
        file = scr.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("G:\\IT\\IV\\Автотестирование ПО\\testWeb\\src\\main\\gotocart.PNG"));
        System.out.println("скрин go to cart is selected сделан \n \n");
        currentUrl = driver.getCurrentUrl();
        for(int i = 0; i<3; i++){
            if(driver.getCurrentUrl().equals("https://wearekpop.com/cart")){
                actual = true;
                break;
            }
            Thread.sleep(700);
        }
        Assert.assertEquals(currentUrl, "https://wearekpop.com/cart", "steps: #8");

        List<WebElement> list = driver.findElements(By.xpath("//div[contains(@class, 'cart-left')]/form//ul[@class='cart-list']//div"));
        list.stream().forEach(elem -> System.out.println(elem.getText().replaceAll("Quantity:", " " ) ));
        list = driver.findElements(By.xpath("//div[contains(@class, 'cart-left')]/form//ul[@class='cart-list']//input[@*]"));
        list.stream().forEach(elem -> System.out.println("num of item = " + elem.getAttribute("value")));

        el = driver.findElement(By.xpath("//li[@id='40632508350660']//a[contains(@class,'remove')]"));
        el.click();
        System.out.println("удаление aespa album \n \n");
        Thread.sleep(2000);
        list = driver.findElements(By.xpath("//div[contains(@class, 'cart-left')]/form//ul[@class='cart-list']//div"));
        list.stream().forEach(elem -> System.out.println(elem.getText().replace("Quantity:", " " )));
        list = driver.findElements(By.xpath("//div[contains(@class, 'cart-left')]/form//ul[@class='cart-list']//input[@*]"));
        list.stream().forEach(elem -> System.out.println("num of item = " + elem.getAttribute("value")));

    }
}
